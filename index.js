require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const pg = require('pg');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const client = new pg.Client({
    connectionString: process.env.DATABASE_URL
  });

client.connect();

app.use(cors());
app.use(express.json());

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.SECRET_TOKEN, (err, data) => {
    if (err) {
      console.log(err);
      return res.sendStatus(403);
    }
    req.body.email = data.email;
    next();
  })
}

function generateToken(email) {
  return jwt.sign(email, process.env.SECRET_TOKEN, { expiresIn: '7d' });
}

function encryptPassword(password) {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
}

app.get('/all-movie',async  (req, res) => {
  const movieQuery = await client.query('SELECT * FROM public.movies ORDER BY release DESC;').catch(err => console.log(err));
  if (movieQuery.rowCount > 1) {
    res.json(movieQuery.rows);
  } else {
    res.sendStatus(500);
  }
});

app.post('/login', async (req, res) => {
  const userData = await client.query(`SELECT * FROM public."user" WHERE email = '${req.body.email}';`).catch(err => console.log(err));
  if (userData.rowCount > 0) {
    bcrypt.compare(req.body.password, userData.rows[0].password, (err, response) => {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      }
      if (response) {
        const token = generateToken({ email: req.body.email });
        res.json({
          id: userData.rows[0].id,
          name: userData.rows[0].name,
          email: userData.rows[0].email,
          membership: userData.rows[0].id_membership,
          token: token
        })
      } else {
        res.sendStatus(401);
      }
    })
  } else {
    res.sendStatus(401);
  }
})

app.post('/register', async (req, res) => {
  const date = new Date;
  const validateUser = await client.query(`SELECT * FROM public."user" WHERE email = '${req.body.email}'`).catch(err => console.log(err));
  if (validateUser.rowCount === 0) {
    const encryptedPassword = encryptPassword(req.body.password);
    const insertUserQuery = await client.query(`INSERT INTO public."user"(name, email, password, created_at)
      VALUES ('${req.body.name}', '${req.body.email}', '${encryptedPassword}', '${date.toUTCString()}' ) RETURNING id;`).catch(err => console.log(err));
    if (insertUserQuery) {
      const token = generateToken({ email: req.body.email });
      res.json({
        id: insertUserQuery.rows[0].id,
        name: req.body.name,
        email: req.body.email,
        membership: null,
        token: token
      })
    } else {
      res.sendStatus(500);
    }
  } else {
    res.sendStatus(401);
  }
})

app.get('/membership', authenticateToken, async (req, res) => {
  let output = {};
  const queryMembership = await client.query('SELECT * FROM public.membership ORDER BY id DESC;').catch(err => console.log(err));
  const queryTransaction = await client.query(`SELECT * FROM public.transaction WHERE id_user = (SELECT id FROM public.user WHERE email = '${req.body.email}');`);
  if (queryMembership.rowCount > 0) {
    output.allMembership = queryMembership.rows;
    if (queryTransaction.rowCount > 0) {
      let totalItem = 0;
      queryTransaction.rows.forEach(data => {
        totalItem += data.qty;
      })
      output.canSubscribe = queryMembership.rows.filter(data => {
        return data.min_order <= totalItem;
      })
    }
    res.json(output);
  } else {
    res.sendStatus(500);
  }
})

app.post('/subscribe', authenticateToken, async (req, res) => {
  const queryUpdate = await client.query(`UPDATE public."user" SET id_membership=${req.body.id_membership} WHERE email='${req.body.email}' RETURNING id;`).catch(err => console.log(err));
  if (queryUpdate.rowCount > 0) {
    res.sendStatus(200);
  } else {
    res.sendStatus(500);
  }
})

app.get('/user', authenticateToken, async (req, res) => {
  const queryUser = await client.query(`SELECT * FROM public."user" WHERE email = '${req.body.email}';`).catch(err => console.log(err));
  if (queryUser.rowCount > 0) {
    res.json({
      id: queryUser.rows[0].id,
      name: queryUser.rows[0].name,
      email: req.body.email,
      membership: queryUser.rows[0].id_membership
    })
  } else {
    res.sendStatus(401);
  }
})

app.post('/checkout', authenticateToken, async (req, res) => {
  const date = new Date;
  const createdAt = date.toUTCString();
  const transactionId = Date.now();
  if (req.body.items) {
    const movieData = req.body.items;
    const userData = await client.query(`SELECT * FROM public."user" WHERE email = '${req.body.email}';`).catch(err => console.log(err));
    await movieData.forEach(async data => {
      const quesyCheckout = await client.query(`INSERT INTO public.transaction(id_transaction, id_user, id_movie, discount, created_at, qty)
        VALUES ('${transactionId}', ${userData.rows[0].id}, '${data.item.id}', ${req.body.discount}, '${createdAt}', ${data.qty});`).catch(err => console.log(err));
    });
    res.json({ id_transaction: transactionId });
  } else {
    res.sendStatus(500);
  }
})

app.get('/transaction', async (req, res) => {
  const idTransaction = req.query.id_transaction;
  const queryTransaction = await client.query(`SELECT * FROM public.transaction WHERE id_transaction='${idTransaction}';`).catch(err => console.log(err));
  const movieQuery = await client.query('SELECT * FROM public.movies;').catch(err => console.log(err));
  if (queryTransaction.rowCount > 0 && movieQuery.rowCount > 0) {
    const idMovieTransaction = queryTransaction.rows.map(data => data.id_movie);
    const movieDataTransaction = movieQuery.rows.filter(data => idMovieTransaction.includes(data.id));
    const itemsTransaction = queryTransaction.rows.map(data => {
      const itemMovie = movieQuery.rows.find(element => element.id === data.id_movie);
      return { item: itemMovie, qty: data.qty }
    })

    const result = {
      id: queryTransaction.rows[0].id_transaction,
      items: itemsTransaction,
      discount: queryTransaction.rows[0].discount,
      created_at: queryTransaction.rows[0].created_at
    }
    res.json(result);
  } else {
    res.sendStatus(500);
  }

})

app.listen(process.env.PORT || 5000, () => console.log(`Server Runing in ${process.env.PORT || 5000}`));
